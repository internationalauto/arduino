// Code for Arduio 2 (4 tof sensors)
// Author: Nishant Jain

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X FL45; // SHUTDOWN PIN 8, ADDRESS 22
VL53L0X F45R; //SHUTDOWN PIN 9, ADDRESS 25
VL53L0X BR45; //SHUTDOWN PIN 10, ADDRESS 28
VL53L0X B45L; //SHUTDOWN PIN 11, ADDRESS 31

uint8_t FL45_PIN_SHUT = 8;
uint8_t F45R_PIN_SHUT = 9; 
uint8_t BR45_PIN_SHUT = 10; 
uint8_t B45L_PIN_SHUT = 11;   

void setup()
{
  pinMode(FL45_PIN_SHUT, OUTPUT);
  pinMode(F45R_PIN_SHUT, OUTPUT);
  pinMode(BR45_PIN_SHUT, OUTPUT);
  pinMode(B45L_PIN_SHUT, OUTPUT);
  
  digitalWrite(FL45_PIN_SHUT, LOW);
  digitalWrite(F45R_PIN_SHUT, LOW);
  digitalWrite(BR45_PIN_SHUT, LOW);
  digitalWrite(B45L_PIN_SHUT, LOW);

  delay(500);
  Wire.begin();


  Serial.begin(9600);
  Serial.println("STARTING UP");

  
  //SENSOR FL45
  pinMode(FL45_PIN_SHUT, INPUT);
  delay(200);
  FL45.init(true);
  delay(150);
  FL45.setAddress((uint8_t)22);
  
  Serial.println("FL45 Address set");
  
  //SENSOR F45R
  pinMode(F45R_PIN_SHUT, INPUT);
  delay(200);
  F45R.init(true);
  delay(150);
  F45R.setAddress((uint8_t)25);
  
  Serial.println("F45R Address set");

  //SENSOR BR45
  pinMode(BR45_PIN_SHUT, INPUT);
  delay(200);
  BR45.init(true);
  delay(150);
  BR45.setAddress((uint8_t)28);
  
  Serial.println("BR45 Address set");

  //SENSOR B45L
  pinMode(B45L_PIN_SHUT, INPUT);
  delay(200);
  B45L.init(true);
  delay(150);
  B45L.setAddress((uint8_t)31);
  
  Serial.println("B45L Address set");

  FL45.setTimeout(500);
  F45R.setTimeout(500);
  BR45.setTimeout(500);
  B45L.setTimeout(500);

  // Initialize system
  Serial.println("__________________________________________________________________");
  Serial.println("");
  Serial.println("=================================");
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  //for (byte i = 1; i < 120; i++)
  for (byte i = 1; i < 30; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
  Serial.println("=================================");
}


void measureAndPrintDistance(VL53L0X sensor, char sensorName[]) {
  char meas[12];
  long distance = sensor.readRangeSingleMillimeters();
  if (sensor.timeoutOccurred()) {
    sprintf(meas, "%s,0000", sensorName);
  } else {
    sprintf(meas, "%s,%ld", sensorName, distance);
  }
  Serial.println(meas);
}


void loop()
{
  
  // To DECODE, split the received string into items[]
  // item[0] will represent the command/sensor name
  // item[1] will represent the value/status

  unsigned long currentMillis = millis();

  measureAndPrintDistance(FL45, "TOF_FL45");
  measureAndPrintDistance(F45R, "TOF_F45R");
  measureAndPrintDistance(BR45, "TOF_BR45");
  measureAndPrintDistance(B45L, "TOF_B45L");


  unsigned long timeTaken = millis() - currentMillis;
  
  Serial.print("Total time taken (ms): ");
  Serial.println(timeTaken);

  delay(500);
}
