// Code for Arduio 4 (4 tof sensors)
// Author: Nishant Jain

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X FR45; // SHUTDOWN PIN 8, ADDRESS 22
VL53L0X B45R; //SHUTDOWN PIN 9, ADDRESS 25
VL53L0X BL45; //SHUTDOWN PIN 10, ADDRESS 28
VL53L0X F45L; //SHUTDOWN PIN 11, ADDRESS 31

uint8_t FR45_PIN_SHUT = 8;
uint8_t B45R_PIN_SHUT = 9; 
uint8_t BL45_PIN_SHUT = 10; 
uint8_t F45L_PIN_SHUT = 11;   

void setup()
{
  pinMode(FR45_PIN_SHUT, OUTPUT);
  pinMode(B45R_PIN_SHUT, OUTPUT);
  pinMode(BL45_PIN_SHUT, OUTPUT);
  pinMode(F45L_PIN_SHUT, OUTPUT);
  
  digitalWrite(FR45_PIN_SHUT, LOW);
  digitalWrite(B45R_PIN_SHUT, LOW);
  digitalWrite(BL45_PIN_SHUT, LOW);
  digitalWrite(F45L_PIN_SHUT, LOW);

  delay(500);
  Wire.begin();


  Serial.begin(9600);
  Serial.println("STARTING UP");

  
  //SENSOR FR45
  pinMode(FR45_PIN_SHUT, INPUT);
  delay(200);
  FR45.init(true);
  delay(150);
  FR45.setAddress((uint8_t)22);
  
  Serial.println("FR45 Address set");
  
  //SENSOR B45R
  pinMode(B45R_PIN_SHUT, INPUT);
  delay(200);
  B45R.init(true);
  delay(150);
  B45R.setAddress((uint8_t)25);
  
  Serial.println("B45R Address set");

  //SENSOR BL45
  pinMode(BL45_PIN_SHUT, INPUT);
  delay(200);
  BL45.init(true);
  delay(150);
  BL45.setAddress((uint8_t)28);
  
  Serial.println("BL45 Address set");

  //SENSOR F45L
  pinMode(F45L_PIN_SHUT, INPUT);
  delay(200);
  F45L.init(true);
  delay(150);
  F45L.setAddress((uint8_t)31);
  
  Serial.println("F45L Address set");

  FR45.setTimeout(500);
  B45R.setTimeout(500);
  BL45.setTimeout(500);
  F45L.setTimeout(500);

  // Initialize system
  Serial.println("__________________________________________________________________");
  Serial.println("");
  Serial.println("=================================");
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  //for (byte i = 1; i < 120; i++)
  for (byte i = 1; i < 30; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
  Serial.println("=================================");
}


void measureAndPrintDistance(VL53L0X sensor, char sensorName[]) {
  char meas[12];
  long distance = sensor.readRangeSingleMillimeters();
  if (sensor.timeoutOccurred()) {
    sprintf(meas, "%s,0000", sensorName);
  } else {
    sprintf(meas, "%s,%ld", sensorName, distance);
  }
  Serial.println(meas);
}


void loop()
{
  
  // To DECODE, split the received string into items[]
  // item[0] will represent the command/sensor name
  // item[1] will represent the value/status

  unsigned long currentMillis = millis();

  measureAndPrintDistance(FR45, "TOF_FR45");
  measureAndPrintDistance(B45R, "TOF_B45R");
  measureAndPrintDistance(BL45, "TOF_BL45");
  measureAndPrintDistance(F45L, "TOF_F45L");


  unsigned long timeTaken = millis() - currentMillis;
  
  Serial.print("Total time taken (ms): ");
  Serial.println(timeTaken);

  delay(500);
}
