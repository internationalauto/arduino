// Code for Arduio 1 (4 tof sensors)
// Author: Nishant Jain

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X FL; // SHUTDOWN PIN 8, ADDRESS 22
VL53L0X RF; //SHUTDOWN PIN 9, ADDRESS 25
VL53L0X BR; //SHUTDOWN PIN 10, ADDRESS 28
VL53L0X LB; //SHUTDOWN PIN 11, ADDRESS 31

uint8_t FL_PIN_SHUT = 8;
uint8_t RF_PIN_SHUT = 9; 
uint8_t BR_PIN_SHUT = 10; 
uint8_t LB_PIN_SHUT = 11;   

void setup()
{
  pinMode(FL_PIN_SHUT, OUTPUT);
  pinMode(RF_PIN_SHUT, OUTPUT);
  pinMode(BR_PIN_SHUT, OUTPUT);
  pinMode(LB_PIN_SHUT, OUTPUT);
  
  digitalWrite(FL_PIN_SHUT, LOW);
  digitalWrite(RF_PIN_SHUT, LOW);
  digitalWrite(BR_PIN_SHUT, LOW);
  digitalWrite(LB_PIN_SHUT, LOW);

  delay(500);
  Wire.begin();


  Serial.begin(9600);
  Serial.println("STARTING UP");

  
  //SENSOR FL
  pinMode(FL_PIN_SHUT, INPUT);
  delay(200);
  FL.init(true);
  delay(150);
  FL.setAddress((uint8_t)22);
  
  Serial.println("FL Address set");
  
  //SENSOR RF
  pinMode(RF_PIN_SHUT, INPUT);
  delay(200);
  RF.init(true);
  delay(150);
  RF.setAddress((uint8_t)25);
  
  Serial.println("RF Address set");

  //SENSOR BR
  pinMode(BR_PIN_SHUT, INPUT);
  delay(200);
  BR.init(true);
  delay(150);
  BR.setAddress((uint8_t)28);
  
  Serial.println("BR Address set");

  //SENSOR LB
  pinMode(LB_PIN_SHUT, INPUT);
  delay(200);
  LB.init(true);
  delay(150);
  LB.setAddress((uint8_t)31);
  
  Serial.println("LB Address set");

  FL.setTimeout(500);
  RF.setTimeout(500);
  BR.setTimeout(500);
  LB.setTimeout(500);

  // Initialize system
  Serial.println("__________________________________________________________________");
  Serial.println("");
  Serial.println("=================================");
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  //for (byte i = 1; i < 120; i++)
  for (byte i = 1; i < 30; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
  Serial.println("=================================");
}


void measureAndPrintDistance(VL53L0X sensor, char sensorName[]) {
  char meas[12];
  long distance = sensor.readRangeSingleMillimeters();
  if (sensor.timeoutOccurred()) {
    sprintf(meas, "%s,0000", sensorName);
  } else {
    sprintf(meas, "%s,%ld", sensorName, distance);
  }
  Serial.println(meas);
}


void loop()
{
  
  // To DECODE, split the received string into items[]
  // item[0] will represent the command/sensor name
  // item[1] will represent the value/status

  unsigned long currentMillis = millis();

  measureAndPrintDistance(FL, "TOF_FL");
  measureAndPrintDistance(RF, "TOF_RF");
  measureAndPrintDistance(BR, "TOF_BR");
  measureAndPrintDistance(LB, "TOF_LB");


  unsigned long timeTaken = millis() - currentMillis;
  
  Serial.print("Total time taken (ms): ");
  Serial.println(timeTaken);

  delay(500);
}
