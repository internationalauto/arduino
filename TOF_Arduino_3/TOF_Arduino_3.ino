// Code for Arduio 3 (4 tof sensors)
// Author: Nishant Jain

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X FR; // SHUTDOWN PIN 8, ADDRESS 22
VL53L0X RB; //SHUTDOWN PIN 9, ADDRESS 25
VL53L0X BL; //SHUTDOWN PIN 10, ADDRESS 28
VL53L0X LF; //SHUTDOWN PIN 11, ADDRESS 31

uint8_t FR_PIN_SHUT = 8;
uint8_t RB_PIN_SHUT = 9; 
uint8_t BL_PIN_SHUT = 10; 
uint8_t LF_PIN_SHUT = 11;   

void setup()
{
  pinMode(FR_PIN_SHUT, OUTPUT);
  pinMode(RB_PIN_SHUT, OUTPUT);
  pinMode(BL_PIN_SHUT, OUTPUT);
  pinMode(LF_PIN_SHUT, OUTPUT);
  
  digitalWrite(FR_PIN_SHUT, LOW);
  digitalWrite(RB_PIN_SHUT, LOW);
  digitalWrite(BL_PIN_SHUT, LOW);
  digitalWrite(LF_PIN_SHUT, LOW);

  delay(500);
  Wire.begin();


  Serial.begin(9600);
  Serial.println("STARTING UP");

  
  //SENSOR FR
  pinMode(FR_PIN_SHUT, INPUT);
  delay(200);
  FR.init(true);
  delay(150);
  FR.setAddress((uint8_t)22);
  
  Serial.println("FR Address set");
  
  //SENSOR RB
  pinMode(RB_PIN_SHUT, INPUT);
  delay(200);
  RB.init(true);
  delay(150);
  RB.setAddress((uint8_t)25);
  
  Serial.println("RB Address set");

  //SENSOR BL
  pinMode(BL_PIN_SHUT, INPUT);
  delay(200);
  BL.init(true);
  delay(150);
  BL.setAddress((uint8_t)28);
  
  Serial.println("BL Address set");

  //SENSOR LF
  pinMode(LF_PIN_SHUT, INPUT);
  delay(200);
  LF.init(true);
  delay(150);
  LF.setAddress((uint8_t)31);
  
  Serial.println("LF Address set");

  FR.setTimeout(500);
  RB.setTimeout(500);
  BL.setTimeout(500);
  LF.setTimeout(500);

  // Initialize system
  Serial.println("__________________________________________________________________");
  Serial.println("");
  Serial.println("=================================");
  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  //for (byte i = 1; i < 120; i++)
  for (byte i = 1; i < 30; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");
  Serial.println("=================================");
}


void measureAndPrintDistance(VL53L0X sensor, char sensorName[]) {
  char meas[12];
  long distance = sensor.readRangeSingleMillimeters();
  if (sensor.timeoutOccurred()) {
    sprintf(meas, "%s,0000", sensorName);
  } else {
    sprintf(meas, "%s,%ld", sensorName, distance);
  }
  Serial.println(meas);
}


void loop()
{
  
  // To DECODE, split the received string into items[]
  // item[0] will represent the command/sensor name
  // item[1] will represent the value/status

  unsigned long currentMillis = millis();

  measureAndPrintDistance(FR, "TOF_FR");
  measureAndPrintDistance(RB, "TOF_RB");
  measureAndPrintDistance(BL, "TOF_BL");
  measureAndPrintDistance(LF, "TOF_LF");


  unsigned long timeTaken = millis() - currentMillis;
  
  Serial.print("Total time taken (ms): ");
  Serial.println(timeTaken);

  delay(500);
}
